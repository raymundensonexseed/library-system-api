<?php

Route::group(['prefix' => 'users'], function(){
    Route::get('/', 'UserController@getUsers');

    Route::get('/{id}', 'UserController@getUsers');

    Route::post('/create','UserController@createUser');

    Route::post('/update/{id}','UserController@updateUser');

    Route::get('/delete/{id}','UserController@deleteUser');
});

Route::group(['prefix' => 'usertypes'], function(){
    Route::get('/', 'UserTypeController@getUserTypes');

    Route::get('/{id}', 'UserTypeController@getUserTypes');

    Route::post('/create','UserTypeController@createUserType');

    Route::post('/update/{id}','UserTypeController@updateUserType');

    Route::get('/delete/{id}','UserTypeController@deleteUserType');
});
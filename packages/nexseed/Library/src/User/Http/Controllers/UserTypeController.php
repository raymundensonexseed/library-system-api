<?php namespace Nexseed\Library\User\Http\Controllers;

use App\Http\Controllers\Controller;
use Nexseed\Library\User\Contracts\UserTypeInterface;
use Response,Request;

class UserTypeController extends Controller{

	public function renderResponse($data,$message){
		echo Response::json([ 'data' => $data , 'message' => $message ]);
	}

	public function getUserTypes(UserTypeInterface $usertype,$id = null){
		if($id == null){
			$data = $usertype->getAllUserTypes();
		}
		else{
			$data = $usertype->getByIdUserType($id);
		}
		$message = !empty($data) ? "Success retrieval!" : "No record found!";
		$this->renderResponse($data,$message);
	}

	public function createUserType(UserTypeInterface $usertype){
		$form_data = Request::all();

		$message = $usertype->createUserType($form_data) ? "Success!" : "Error!";
		$this->renderResponse([],$message);
	}

	public function updateUserType(UserTypeInterface $usertype,$id){
		$form_data = Request::all();
		$message = $usertype->updateUserType($id,$form_data) ? "Success!" : "Error!";
		$this->renderResponse([],$message);
	}

	public function deleteUserType(UserTypeInterface $usertype,$id){
		$message = $usertype->deleteUserType($id) ? "Success!" : "Error!";
		$this->renderResponse([],$message);
	}

}
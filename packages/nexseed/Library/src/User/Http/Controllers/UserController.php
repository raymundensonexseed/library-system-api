<?php namespace Nexseed\Library\User\Http\Controllers;

use App\Http\Controllers\Controller;
use Nexseed\Library\User\Contracts\UserInterface;
use Response,Request,Hash;

class UserController extends Controller{

	public function renderResponse($data,$message){
		echo Response::json([ 'data' => $data , 'message' => $message ]);
	}

	public function getUsers(UserInterface $user,$id = null){
		if($id == null){
			$data = $user->getAllUsers();
		}
		else{
			$data = $user->getByIdUser($id);
		}
		$message = !empty($data) ? "Success retrieval!" : "No record found!";
		$this->renderResponse($data,$message);
	}

	public function createUser(UserInterface $user){
		$form_data = Request::all();
		$form_data['password'] = Hash::make($form_data['password']);

		$message = $user->createUser($form_data) ? "Success!" : "Error!";
		$this->renderResponse([],$message);
	}

	public function updateUser(UserInterface $user,$id){
		$form_data = Request::all();
		$form_data['password'] = Hash::make($form_data['password']);
		
		$message = $user->updateUser($id,$form_data) ? "Success!" : "Error!";
		$this->renderResponse([],$message);
	}

	public function deleteUser(UserInterface $user,$id){
		$message = $user->deleteUser($id) ? "Success!" : "Error!";
		$this->renderResponse([],$message);
	}

}

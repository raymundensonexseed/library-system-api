<?php namespace Nexseed\Library\User\Contracts;

interface UserInterface{

	public function createUser($inputs);

	public function updateUser($user_id,$inputs);

	public function deleteUser($user_id);

	public function getByIdUser($user_id);

	public function getAllUsers($retrieve);

}
<?php namespace Nexseed\Library\User\Contracts;

interface UserTypeInterface{

	public function createUserType($inputs);

	public function updateUserType($user_type_id,$inputs);

	public function deleteUserType($user_type_id);

	public function getByIdUserType($user_type_id);

	public function getAllUserTypes($retrieve);

}
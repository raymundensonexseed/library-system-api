<?php namespace Nexseed\Library\User;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class UserServiceProvider extends ServiceProvider {

    public function boot(){
        $this->setupRoutes($this->app->router);
    }

    public function setupRoutes(Router $router){
        $router->group(['namespace' => 'Nexseed\Library\User\Http\Controllers'], function($router)
        {
            require __DIR__.'/Http/routes.php';
        });
    }

    public function register(){
        $this->app->bind('user',function($app){
            return new User($app);
        });
        $this->bindRepositories();
    }

    public function bindRepositories(){
        $this->app->bind(
            'Nexseed\Library\User\Contracts\UserInterface',
            'Nexseed\Library\User\Services\UserRepository'
        );

        $this->app->bind(
            'Nexseed\Library\User\Contracts\UserTypeInterface',
            'Nexseed\Library\User\Services\UserRepository'
        );
    }
}
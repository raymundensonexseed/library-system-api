<?php namespace Nexseed\Library\User\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model{

	protected $table = 'users';

	protected $fillable = ['name', 'user_type_id', 'password', 'email', 'contact_number', 'contact_number'];

	protected $hidden = ['password', 'remember_token'];

	use SoftDeletes;

	protected $dates = ['deleted_at'];
	
}
<?php namespace Nexseed\Library\User\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserType extends Model{
	
	protected $table = 'user_types';

	protected $fillable = ['name','description'];

	use SoftDeletes;

	protected $dates = ['deleted_at'];

}
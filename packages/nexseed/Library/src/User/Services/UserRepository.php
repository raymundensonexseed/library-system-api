<?php namespace Nexseed\Library\User\Services;

use Nexseed\Library\User\Models\User;
use Nexseed\Library\User\Models\UserType;
use Nexseed\Library\User\Contracts\UserInterface;
use Nexseed\Library\User\Contracts\UserTypeInterface;

class UserRepository implements UserInterface,UserTypeInterface{

	//User Functionalities
	public function createUser($inputs){
		return User::create($inputs);
	}

	public function updateUser($user_id,$inputs){
		return User::where('id','=',$user_id)->update($inputs);
	}

	public function deleteUser($user_id){
		return User::where('id','=',$user_id)->delete();
	}

	public function getByIdUser($user_id){
		$data = User::find($user_id);
		if(count($data) > 0){
			return $data->toArray();
		}
		return [];
	}

	public function getAllUsers($retrieve = 15){
		$data = User::paginate($retrieve);
		if(count($data) > 0){
			return $data->toArray();
		}
		return [];
	}	

	//User Type Functionalities
	public function createUserType($inputs){
		return UserType::create($inputs);
	}

	public function updateUserType($user_type_id,$inputs){
		return UserType::where('id','=',$user_type_id)->update($inputs);
	}

	public function deleteUserType($user_type_id){
		return UserType::where('id','=',$user_type_id)->delete();
	}

	public function getByIdUserType($user_type_id){
		$data = UserType::find($user_type_id);
		return $data;
	}

	public function getAllUserTypes($retrieve = 15){
		$data = UserType::paginate($retrieve);
		if(count($data) > 0){
			return $data->toArray();
		}
		return [];
	}

}
<?php namespace Nexseed\Library\Book\Http\Controllers;

use App\Http\Controllers\Controller;
use Nexseed\Library\Book\Contracts\BookCategoryInterface;
use Response,Request;

class BookCategoryController extends Controller{
	
	public function renderResponse($data,$message){
		echo Response::json([ 'data' => $data , 'message' => $message ]);
	}

	public function getBookCategories(BookCategoryInterface $category,$id = null){
		if($id === null){
			$data = $category->getAllBookCategories();
		}
		else{
			$data = $category->getByIdBookCategory($id);
		}
		$message = !empty($data) ? "Success retrieval!" : "No record found!";
		$this->renderResponse($data,$message);
	}

	public function createBookCategory(BookCategoryInterface $category){
		$form_data = Request::all();
		$message = $category->createBookCategory($form_data) ? "Success!" : "Error!";
		$this->renderResponse([],$message);
	}
	
	public function updateBookCategory(BookCategoryInterface $category,$id){
		$form_data = Request::all();
		$message = $category->updateBookCategory($id,$form_data) ? "Success!" : "Error!";
		$this->renderResponse([],$message);
	}
	
	public function deleteBookCategory(BookCategoryInterface $category,$id){
		$message = $category->deleteBookCategory($id) ? "Success!" : "Error!";
		$this->renderResponse([],$message);
	}
}
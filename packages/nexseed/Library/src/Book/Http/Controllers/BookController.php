<?php namespace Nexseed\Library\Book\Http\Controllers;

use App\Http\Controllers\Controller;
use Nexseed\Library\Book\Contracts\BookInterface;
use Response,Request;

class BookController extends Controller{

	public function renderResponse($data,$message){
		echo Response::json([ 'data' => $data , 'message' => $message ]);
	}

	public function getBooks(BookInterface $book,$id = null){
		if($id == null){
			$data = $book->getAllBooks();
		}
		else{
			$data = $book->getByIdBook($id);
		}
		$message = !empty($data) ? "Success retrieval!" : "No record found!";
		$this->renderResponse($data,$message);
	}

	public function createBook(BookInterface $book){
		$form_data = Request::all();
		$message = $book->createBook($form_data) ? "Success!" : "Error!";
		$this->renderResponse([],$message);
	}

	public function updateBook(BookInterface $book,$id){
		$form_data = Request::all();
		$message = $book->updateBook($id,$form_data) ? "Success!" : "Error!";
		$this->renderResponse([],$message);
	}
	
	public function deleteBook(BookInterface $book,$id){
		$message = $book->deleteBook($id) ? "Success!" : "Error!";
		$this->renderResponse([],$message);
	}
}
<?php

Route::group(['prefix' => 'books'], function(){
    Route::get('/', 'BookController@getBooks');

    Route::get('/{id}', 'BookController@getBooks');

    Route::post('/create','BookController@createBook');

    Route::post('/update/{id}','BookController@updateBook');

    Route::get('/delete/{id}','BookController@deleteBook');

});

Route::group(['prefix' => 'bookcategories'], function(){
    Route::get('/', 'BookCategoryController@getBookCategories');

    Route::get('/{id}', 'BookCategoryController@getBookCategories');

    Route::post('/create','BookCategoryController@createBookCategory');

    Route::post('/update/{id}','BookCategoryController@updateBookCategory');

    Route::get('/delete/{id}','BookCategoryController@deleteBookCategory');
});
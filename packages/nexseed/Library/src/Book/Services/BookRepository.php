<?php namespace Nexseed\Library\Book\Services;

use Nexseed\Library\Book\Models\Book;
use Nexseed\Library\Book\Models\BookCategory;
use Nexseed\Library\Book\Contracts\BookInterface;
use Nexseed\Library\Book\Contracts\BookCategoryInterface;

class BookRepository implements BookInterface,BookCategoryInterface{

	//Book Functionalities
	public function createBook($inputs){
		return Book::create($inputs);
	}

	public function updateBook($book_id,$inputs){
		return Book::where('id','=',$book_id)->update($inputs);
	}

	public function deleteBook($book_id){
		return Book::where('id','=',$book_id)->delete();
	}

	public function getByIdBook($book_id){
		$data = Book::find($book_id);
		if(count($data) > 0){
			return $data->toArray();
		}
		return [];
	}

	public function getAllBooks($retrieve = 15){
		$data = Book::paginate($retrieve);
		if(count($data) > 0){
			return $data->toArray();
		}
		return [];
	}

	//BookCategory Functionalities
	public function createBookCategory($inputs){
		return BookCategory::create($inputs);
	}

	public function updateBookCategory($book_category_id,$inputs){
		return BookCategory::where('id','=',$book_category_id)->update($inputs);
	}

	public function deleteBookCategory($book_category_id){
		return BookCategory::where('id','=',$book_category_id)->delete();
	}

	public function getByIdBookCategory($book_category_id){
		$data = BookCategory::find($book_category_id);
		if(count($data) > 0){
			return $data->toArray();
		}
		return [];
	}

	public function getAllBookCategories($retrieve = 15){
		$data = BookCategory::paginate($retrieve);
		if(count($data) > 0){
			return $data->toArray();
		}
		return [];
	}

}
<?php namespace Nexseed\Library\Book\Contracts;

interface BookCategoryInterface{

	public function createBookCategory($inputs);

	public function updateBookCategory($book_category_id,$inputs);

	public function deleteBookCategory($book_category_id);

	public function getByIdBookCategory($book_category_id);

	public function getAllBookCategories($retrieve);

}
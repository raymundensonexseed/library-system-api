<?php namespace Nexseed\Library\Book\Contracts;

interface BookInterface{

	public function createBook($inputs);

	public function updateBook($book_id,$inputs);

	public function deleteBook($book_id);

	public function getByIdBook($book_id);

	public function getAllBooks($retrieve);

}
<?php namespace Nexseed\Library\Book;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class BookServiceProvider extends ServiceProvider {

    public function boot(){
        $this->setupRoutes($this->app->router);
    }

    public function setupRoutes(Router $router){
        $router->group(['namespace' => 'Nexseed\Library\Book\Http\Controllers'], function($router)
        {
            require __DIR__.'/Http/routes.php';
        });
    }

    public function register(){
        $this->app->bind('book',function($app){
            return new Book($app);
        });
        $this->bindRepositories();
    }

    public function bindRepositories(){
        $this->app->bind(
            'Nexseed\Library\Book\Contracts\BookInterface',
            'Nexseed\Library\Book\Services\BookRepository'
        );

        $this->app->bind(
            'Nexseed\Library\Book\Contracts\BookCategoryInterface',
            'Nexseed\Library\Book\Services\BookRepository'
        );
    }
}
<?php namespace Nexseed\Library\Book\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model{
	protected $table = 'books';

	protected $fillable = array('name', 'isbn', 'book_category_id','author','date_published','count');
}
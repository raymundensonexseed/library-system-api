<?php namespace Nexseed\Library\Book\Models;

use Illuminate\Database\Eloquent\Model;

class BookCategory extends Model{
	protected $table = 'book_categories';

	protected $fillable = array('category', 'description');
}
<?php namespace Nexseed\Library\Borrow\Services;

use Nexseed\Library\Borrow\Models\Borrow;
use Nexseed\Library\Borrow\Contracts\BorrowInterface;
use DB;

class BorrowRepository implements BorrowInterface{

	public function createBorrow($inputs){
		return Borrow::insert($inputs);
	}

	public function updateBorrowBook($book_id,$inputs){
		return Borrow::where('id','=',$book_id)->update($inputs);
	}

	public function cancelBorrowBook($book_id){

	}

	public function cancelAllBorrowBook(){

	}

	public function reserveBook($book_id){

	}

	public function getAllBookBorrowByUser($user_id,$status,$retrieve = 15){
		return Borrow::where('user_id','=',$user_id)
			->where('status','=',$status)
			->paginate($retrieve);
	}

	public function getAllBookBorrower($status,$options = "",$retrieve = 15){
		if($options === "late"){
			$data = Borrow::where('status','=',$status)
				->select(DB::raw('DISTINCT(user_id)'))
				->paginate($retrieve);

		}else{
			$data = Borrow::where('status','=',$status)
				->select(DB::raw('DISTINCT(user_id)'))
				->paginate($retrieve);
		}
		if(count($data) > 0){
			return $data->toArray();
		}
		return [];
	}

}
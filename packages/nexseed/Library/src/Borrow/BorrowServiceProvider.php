<?php namespace Nexseed\Library\Borrow;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class BorrowServiceProvider extends ServiceProvider {

    public function boot(){
        $this->setupRoutes($this->app->router);
    }

    public function setupRoutes(Router $router){
        $router->group(['namespace' => 'Nexseed\Library\Borrow\Http\Controllers'], function($router)
        {
            require __DIR__.'/Http/routes.php';
        });
    }

    public function register(){
        $this->app->bind('borrow',function($app){
            return new Borrow($app);
        });
        $this->bindRepositories();
    }

    public function bindRepositories(){
        $this->app->bind(
            'Nexseed\Library\Borrow\Contracts\BorrowInterface',
            'Nexseed\Library\Borrow\Services\BorrowRepository'
        );
    }
}
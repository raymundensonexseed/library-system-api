<?php namespace Nexseed\Library\Borrow\Models;

use Illuminate\Database\Eloquent\Model;

class Borrow extends Model{
	protected $table = 'borrows';

	protected $fillable = array('book_id', 'user_id', 'borrow_start','borrow_end','status','returned_date');
}
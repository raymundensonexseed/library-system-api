<?php namespace Nexseed\Library\Borrow\Contracts;

interface BorrowInterface{

	public function createBorrow($inputs);

	public function updateBorrowBook($book_id,$inputs);

	public function cancelBorrowBook($book_id);

	public function cancelAllBorrowBook();

	public function reserveBook($book_id);

	public function getAllBookBorrowByUser($user_id,$status,$retrieve);

	public function getAllBookBorrower($status,$options,$retrieve);

}
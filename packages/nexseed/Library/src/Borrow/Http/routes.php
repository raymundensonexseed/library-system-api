<?php

Route::group(['prefix' => 'borrow'], function(){
	Route::post('/create', 'BorrowController@createBorrow');

	Route::post('/update/{id}', 'BorrowController@updateBorrowBook');

	Route::group(['prefix' => '/users'], function(){
		Route::get('/pending', 'BorrowController@pendingBorrowers');
		Route::get('/on-going', 'BorrowController@ongoingBorrowers');
		Route::get('/late', 'BorrowController@ongoingBorrowers');
	});
});
<?php namespace Nexseed\Library\Borrow\Http\Controllers;

use App\Http\Controllers\Controller;
use Nexseed\Library\Borrow\Contracts\BorrowInterface;
use Response,Request;

class BorrowController extends Controller{
	
	public function renderResponse($data,$message){
		echo Response::json([ 'data' => $data , 'message' => $message ]);
	}

	public function createBorrow(BorrowInterface $borrow){
		$form_data = Request::all();

		foreach ($form_data['books'] as $book) {
			$borrows[] = [
				'book_id' => $book,
				'user_id' => $form_data['user_id'],
				'status' => 1
			];
		}

		$message = $borrow->createBorrow($borrows) ? "Success!" : "Error!";
		$this->renderResponse([],$message);
	}

	public function updateBorrowBook(BorrowInterface $borrow,$id){
		$form_data = Request::all();
		$message = $borrow->updateBorrowBook($id,$form_data) ? "Success!" : "Error!";
		$this->renderResponse([],$message);
	}

	public function pendingBorrowers(BorrowInterface $borrow){
		$data = $borrow->getAllBookBorrower(1);
		$message = !empty($data) ? "Success retrieval!" : "No record found!";
		$this->renderResponse($data,$message);
	}

	public function ongoingBorrowers(BorrowInterface $borrow){
		$data = $borrow->getAllBookBorrower(2);
		$message = !empty($data) ? "Success retrieval!" : "No record found!";
		$this->renderResponse($data,$message);
	}

	public function lateBorrowers(BorrowInterface $borrow){
		$data = $borrow->getAllBookBorrower(2,'late');
		$message = !empty($data) ? "Success retrieval!" : "No record found!";
		$this->renderResponse($data,$message);
	}

}